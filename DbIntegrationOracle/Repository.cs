﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntity;

namespace DbIntegrationOracle
{
   public  class Repository:IRepository
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public  List<MemberDetails> GetMemeberInformations(int TeamID)
        {
            return Integration.GetMemeberInformations(TeamID);
        }
        public int ReplaceTeamLeader(string Purpose, int TeamID, int NewTeamLeadID)
        {
            return Integration.ReplaceTeamLeader( Purpose,  TeamID,  NewTeamLeadID);
        }
        public int Login(string username, string password)
        {
            return Integration.Login(username, password);
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntity;

namespace DbIntegrationOracle
{
    public interface IRepository: IDisposable
    {
         List<MemberDetails> GetMemeberInformations(int TeamID);
        int ReplaceTeamLeader(string Purpose, int TeamID, int NewTeamLeadID);
        int Login(string username, string password);
    }
}

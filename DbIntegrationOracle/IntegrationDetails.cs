﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbIntegrationOracle
{
    public class TaskManagementDetails
    {
        public decimal Id { get; set; }
        public string TaskDescription { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public decimal? Completion { get; set; }
        public string Status { get; set; }


    }
    public class AssignedToAssignedByDetails
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
    }

    public class LoginUserDetails
    {
        public decimal UserId { get; set; }
        public string FullName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BusinessEntity;

namespace DbIntegrationOracle
{
    public class Integration
    {
        //Registration task
        public int Register(string username, string email, string password,string Firstname,string Lastname)
        {

            using (var objTM = new OracleDBIntegrationEntities())
            {
                var StatusUserName = objTM.REGISTRATIONs.Any(x => x.USERNAME == username);
                var StatusEmail = objTM.REGISTRATIONs.Any(x => x.EMAIL == email);

                if (StatusUserName)
                {
                    return 0;
                }
                else if (StatusEmail)
                {
                    return 2;
                }
                else  {
                    decimal TopId = objTM.REGISTRATIONs.DefaultIfEmpty().Max(p => p == null ? 0 : p.ID);
                    REGISTRATION reg = new REGISTRATION();
                    reg.USERNAME = username;
                    reg.ID = TopId + 1;
                    reg.EMAIL = email;
                    reg.PASSWORD = password;
                    reg.FIRSTNAME = Firstname;
                    reg.LASTNAME = Lastname;

                    objTM.REGISTRATIONs.Add(reg);

                    objTM.SaveChanges();
                    return 1;
                }
                //var Details = from a in objTM.Registrations
                //              where a.Username == username
                //              select new { a.Username };

                //foreach (var names in Details)
                //{
                //    string abcd = names.Username;
                //}
            }
            //List<Registration> objReg;


        }
        //Login
        public static int Login(string username, string password)
        {

            using (var objTM = new OracleDBIntegrationEntities())
            {
                var Status = objTM.REGISTRATIONs.Any(x => x.USERNAME == username && x.PASSWORD == password);

                if (Status)
                {
                    return 1;
                }
                else {

                    return 0;
                }

            }

        }
        //Update task from task page
        public static int TaskUpdation(TaskManagementDetails objTask)
        {
            using (var objTM = new OracleDBIntegrationEntities())
            {
                TASKDETAIL reg = new TASKDETAIL();
                decimal TopId = objTM.TASKDETAILS.DefaultIfEmpty().Max(p => p == null ? 0 : p.TASKID);
                reg.TASKID = TopId+1;
                reg.TASKDESC = objTask.TaskDescription;
                reg.STARTDATE = Convert.ToDateTime(objTask.StartDate);
                reg.ENDDATE = Convert.ToDateTime(objTask.EndDate);
                reg.ASSIGNEDBY = objTask.AssignedBy;
                reg.ASSIGNEDTO = objTask.AssignedTo;
                reg.TASKSTATUS = Convert.ToDecimal(objTask.Completion);
                objTM.TASKDETAILS.Add(reg);

                objTM.SaveChanges();
                return 1;


            }

        }
        //Get all task details.
        public static List<TaskManagementDetails> GetAllTasks()
        {
            using (var entity = new OracleDBIntegrationEntities())
            {
                //var taskDetails = entity.TASKDETAILS.Select(x => new TaskManagementDetails()
                //{
                //    Id = x.TASKID,
                //    TaskDescription = x.TASKDESC,
                //    StartDate = x.STARTDATE,
                //    EndDate = x.ENDDATE,
                //    AssignedTo = x.ASSIGNEDTO,
                //    AssignedBy = x.ASSIGNEDBY,
                //    Completion = x.TASKSTATUS
                //}).ToList();

                var taskDetails = (from task in entity.TASKDETAILS
                                   join status in entity.TASKSTATUS on task.TASKSTATUS equals status.STATUSID
                                   select new TaskManagementDetails()
                                   {
                                       Id = task.TASKID,
                                       TaskDescription = task.TASKDESC,
                                       StartDate = task.STARTDATE,
                                       EndDate = task.ENDDATE,
                                       AssignedTo = task.ASSIGNEDTO,
                                       AssignedBy = task.ASSIGNEDBY,
                                       Status = status.STATUSNAME,
                                       Completion = task.TASKSTATUS
                                
                            }).ToList();

                return taskDetails;
            }
        }
        //Get all team deatils
        public DataTable GetTeamDetails(int UserID)          //Function to get team details
        {
            using (var entity = new OracleDBIntegrationEntities())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("TeamId", typeof(Int32));
                dt.Columns.Add("TeamName", typeof(string));
                dt.Columns.Add("TeamLeadId", typeof(Int32));
                dt.Columns.Add("TeamLeadName", typeof(string));
                var query = (from user in entity.USERTEAMMAPPINGs
                             join team in entity.TEAMTBLs on user.TEAMID equals team.TEAMID
                             join reg in entity.REGISTRATIONs on team.TEAMLEADID equals reg.ID
                             where user.USERID == UserID
                             select new
                             {
                                 team.TEAMID,
                                 team.TEAMNAME,
                                 team.TEAMLEADID,
                                 reg.FIRSTNAME,
                                 reg.LASTNAME
                             });

                foreach (var e in query)
                {
                    DataRow dr = dt.NewRow();
                    dr["TeamId"] = e.TEAMID;
                    dr["TeamName"] = e.TEAMNAME;
                    dr["TeamLeadId"] = e.TEAMLEADID;
                    dr["TeamLeadName"] = e.FIRSTNAME + " " + e.LASTNAME;
                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }
        //Get all detaiuls of user
        public List<LoginUserDetails> GetUserDetails(string UserName) //Function to get User details
        {
            using (var entity = new OracleDBIntegrationEntities())
            {

                var IdDetails = (from User in entity.REGISTRATIONs
                                 where User.USERNAME == UserName
                                 select new LoginUserDetails()
                                 {
                                     UserId = User.ID,
                                     FullName = User.FIRSTNAME + " " + User.LASTNAME
                                 }).ToList();


                return IdDetails;
            }
        }
        // Get dropdown value for assigned by persons
        public List<AssignedToAssignedByDetails> GetAssignedToDropdownDetails(int TeamId, string AssignedBy) //Function to fill the dropdowns
        {
            using (var entity = new OracleDBIntegrationEntities())
            {

                var DropdwonDetails = (from user in entity.USERTEAMMAPPINGs
                                       join reg in entity.REGISTRATIONs on user.USERID equals reg.ID
                                       where user.TEAMID == TeamId && reg.USERNAME != AssignedBy
                                       select new AssignedToAssignedByDetails()
                                       {
                                           UserName = reg.USERNAME,
                                           FullName = reg.FIRSTNAME + " " + reg.LASTNAME
                                       }).ToList();

                return DropdwonDetails;

            }
        }
        // Get Selected user tasks
        public List<TaskManagementDetails> GetAllTasksForSelectedUser(String AssignedTo)        //Function to get tasks for particular Users
        {

            using (var entity = new OracleDBIntegrationEntities())
            {
                
                var task = entity.TASKDETAILS.Where(x => x.ASSIGNEDTO == AssignedTo).Select(x => new TaskManagementDetails()
                {
                    Id = x.TASKID,
                    TaskDescription = x.TASKDESC,
                    StartDate = x.STARTDATE,
                    EndDate = x.ENDDATE,
                    AssignedTo = x.ASSIGNEDTO,
                    AssignedBy = x.ASSIGNEDBY,
                    Completion = x.TASKSTATUS
                }).ToList();

                return task;
            }

        }
        // Get Memeber Informations 
        public static  List<MemberDetails> GetMemeberInformations(decimal TeamID)
        {
            List<MemberDetails> lstgetMember = new List<MemberDetails>();
            if (TeamID != -1)
            {
                using (var entity = new OracleDBIntegrationEntities())
                {
                    lstgetMember = (
                                    from members in entity.TEAMTBLs
                                    from User in entity.REGISTRATIONs
                                    where members.TEAMID==TeamID && members.TEAMLEADID!=User.ID
                                    && !(from map in entity.USERTEAMMAPPINGs where map.TEAMID==TeamID
                                         select map.USERID).Contains(User.ID)
                                    select new MemberDetails()
                                    {
                                        MemberID = (Int32)User.ID,
                                        MemberName = User.FIRSTNAME + " " + User.LASTNAME

                                    }).ToList().Distinct().ToList();


                    return lstgetMember;
                }
            }
            else
            {
                
                using (var entity = new OracleDBIntegrationEntities())
                {

                    lstgetMember = (from User in entity.REGISTRATIONs

                                    select new MemberDetails()
                                    {
                                        MemberID = (Int32)User.ID,
                                        MemberName = User.FIRSTNAME + " " + User.LASTNAME
                                    }).ToList();


                    return lstgetMember;
                }
            }
           
             
        }
        //Replace Team Leader
        public static int ReplaceTeamLeader(string Purpose, decimal TeamID, decimal NewTeamLeadID)
        {
            try
            {
                using (var objTM = new OracleDBIntegrationEntities())
                {
                    var cust =
                    (from c in objTM.TEAMTBLs
                     where c.TEAMID == TeamID
                     select c).First();
                    cust.TEAMLEADID = NewTeamLeadID;
                    objTM.SaveChanges();
                    return 1;
                }

            }

            catch (Exception e)
            {
                return 0;
            }   

        }
    }
}

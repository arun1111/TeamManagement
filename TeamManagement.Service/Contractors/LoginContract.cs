﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TeamManagement.Service.Contractors
{
    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class TransactionResult
    {
        [DataMember]
        public bool boolTransactionStatus { get; set; }

        [DataMember]
        public string strTransactionException { get; set; }
    }


    [DataContract]
    public class TRLogin : TransactionResult
    {
        [DataMember]
        public bool boolIsValidUser { get; set; }
    }
}
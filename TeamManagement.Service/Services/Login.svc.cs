﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DbIntegrationOracle;
using TeamManagement.Service.Contractors;

namespace TeamManagement.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Login : ILoginService
    {
        private readonly TRLogin LoginTransaction;

        public Login()
        {
            LoginTransaction = new TRLogin();
        }

        public TRLogin GetLoginDetails(string username, string password)
        {
            IRepository ObjRepositary;
            ObjRepositary = new Repository();
            if (ObjRepositary.Login(username, password) == 1)
            {
                LoginTransaction.boolIsValidUser = true;
                LoginTransaction.boolTransactionStatus = true;
            }
            else
            {
                LoginTransaction.boolTransactionStatus = false;
                LoginTransaction.strTransactionException ="Login Failed";
            }
            return LoginTransaction;
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntity
{
    public class MemberDetails
    {
        public string MemberName { get; set; }
        public int MemberID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TeamManagement
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

           
            routes.MapRoute(
                            name: "DeleteTask",
                            url: "TaskManagement/DeleteTask/{pTaskId}",
                            defaults: new { controller = "TaskManagement", action = "DeleteTask", pTaskId = "pTaskId" }
                            );

            routes.MapRoute(
                name: "AddTeam",
                url: "InternalTeamManagemet/AddTeam/{pTeamName}/{pTeamLeadId}",
                defaults:new {controller= "InternalTeamManagemet", action= "AddTeam", pTeamName = "pTeamName", pTeamLeadId= 1//"pTeamLeadId" 
                }
                );

            routes.MapRoute(
                name: "AddMember",
                url: "InternalTeamManagemet/AddTeamMember/{pTeamId}/{pMemberId}",
                defaults: new { controller = "InternalTeamManagemet", action = "AddTeamMember", pTeamId = "pTeamId", pMemberId = "pMemberId" }
                );

            routes.MapRoute(
                name: "TeamRename",
                url: "InternalTeamManagemet/TeamRename/{pTeamId}/{pTeamName}",
                defaults: new { controller = "InternalTeamManagemet", action = "TeamRename", pTeamId = "pTeamId", pTeamName = "pTeamName" }
                );

            routes.MapRoute(
               name: "RemoveMember",
               url: "InternalTeamManagemet/DeleteMember/{pTeamId}/{pMemberId}",
               defaults: new { controller = "InternalTeamManagemet", action = "DeleteMember", pTeamId = "pTeamId", pMemberId = "pMemberId" }
               );

            routes.MapRoute(
               name: "DeleteTeam",
               url: "InternalTeamManagemet/DeleteTeam/{pTeamId}",
               defaults: new { controller = "InternalTeamManagemet", action = "DeleteTeam", pTeamId = "pTeamId" }
               );

            routes.MapRoute(
              name: "Default",
              url: "{controller}/{action}/{id}",
              defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional }
          );

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional }
            //);
        }
    }
}

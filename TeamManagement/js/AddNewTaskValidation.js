﻿

// ------------------------------All Add New Task Validation activities---------------------------------------------------------

$(document).ready(function () {
    $('#gridAddEditDialog_txtTaskId').bind('copy paste cut', function (e) {
        e.preventDefault(); //disable cut,copy,paste
    });
    $('#gridAddEditDialog_txtTaskAssignedBy').bind('copy paste cut', function (e) {
        e.preventDefault(); //disable cut,copy,paste
    });
    $('#gridAddEditDialog_txtTaskAssignedTo').bind('copy paste cut', function (e) {
        e.preventDefault(); //disable cut,copy,paste
    });
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 8 ) {
        return false;
    }
    return true;
}

function ValidateAlphabets(evt) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32 && keyCode!=8)

        return false;
    return true;
}

function validateDate(testdate) {
    var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if (!date_regex.test(testdate)) {
        return false;
    }
    else {
        return true;
    }
}

$(function () {
   
    $("#gridAddEditDialog_dtStartDate").datepicker({
        numberOfMonths: 2,
        showOn: "button",
               buttonImage: "../Images/Calender.png",
               buttonImageOnly: true,
               buttonText: "Select date",

        onSelect: function (selected) {
            var dt = new Date(selected);
            //dt.setDate(dt.getDate());
           
            $("#gridAddEditDialog_dtEndDate").datepicker("option", "minDate", dt);
        }
    });
    $("#gridAddEditDialog_dtEndDate").datepicker({
        numberOfMonths: 2,
        showOn: "button",
        buttonImage: "../Images/Calender.png",
        buttonImageOnly: true,
        buttonText: "Select date",
        onSelect: function (selected) {
            var dt = new Date(selected);
            // dt.setDate(dt.getDate() );
            
               $("#gridAddEditDialog_dtStartDate").datepicker("option", "maxDate", dt);
           
        }
    });
});

$(document).ready(function () {

   
    $('#gridAddEditDialog_txtTaskDescription').keyup(function () {
        var left = 1500 - $(this).val().length;
        if (left < 0) {
            left = 0;
        }
        $('#counter').text('Characters Remaining: ' + left);
    });


});




﻿var newWidth = window.innerWidth * 0.8;
$(document).ready(function () {
    //User with no Team 

    if (SessionTeamLeadID == 0) {
        $("#gridRowAdd").prop("disabled", true);
        $("#divForSelect").hide();

    }
    else {
        $("#gridRowAdd").prop("disabled", false);
        $("#divForSelect").show();
    }

    $("#divMenu").show();
    $('.my_select_box', this).chosen('destroy').chosen();
    $(".my_select_box").chosen({
        disable_search_threshold: 5,
        no_results_text: "Oops, nothing found!",

    });

    $(".logoutcls").show();
    $("#gridAddEditDialog_dtStartDate").prop("disabled", true);
    $("#gridAddEditDialog_dtEndDate").prop("disabled", true);

    $(".clsStartDateDateicker").datepicker();
    $(".clsEndDateDateicker").datepicker();

    $('#TaskManagementDataTable').DataTable({
        "aaSorting": []
        , "aoColumnDefs": [{ "bSortable": false, "aTargets": [7] }]
    });

    $("#gridAddEditDialog").hide();

 
});
//----------------------------------- Team Management Add/Edit functions ---------------------------
var teamMgtURLs =
   {
       addNewTask:  "../TaskManagement/AddNewTask",
       editTask: "../TaskManagement/EditTask",
       deleteTask: "../TaskManagement/DeleteTask",
       SearchUserTask:"../TaskManagement/SearchTask"

   };
$(function () {

    var wWidth = $(window).width();
    var dWidth = wWidth * 0.6;
    var wHeight = $(window).height();
    var dHeight = wHeight * 0.8;
   
        $("#gridAddEditDialog").dialog(
                {

                    width: dWidth,
                    autoOpen: false,
                    height: dHeight,
                    cache: false,
                    modal: true,
                    open: function () {
                        originalContentInDialog = $("#gridAddEditDialog").html();
                    },
                    close: function () {
                        //$(this).remove();
                        //  $("#gridAddEditDialog").html(originalContentInDialog);
                        // $(this).closest('form').find("input[type=text], textarea").val("");
                        // $('#AddEditDialogForm').find("input[type=text], textarea").val("");
                        // $("#gridAddEditDialog_dtStartDate").datepicker();
                        //$("#gridAddEditDialog_dtEndDate").datepicker();
                        clearDialogElements();
                        // $("#gridAddEditDialog_dtStartDate").datepicker("destroy");
                        // $("#gridAddEditDialog_dtEndDate").removeClass('hasDatepicker').datepicker();
                        //$(this).dialog('destroy').remove();
                        $('#gridAddEditDialog').dialog('close');

                    }
                });

        // $("#gridAddEditDialog").dialog("close");
  
    $(document).on("click", "#gridRowAdd", function () {

        $("#gridAddEditDialog_dtStartDate").datepicker();
        $("#gridAddEditDialog_dtEndDate").datepicker();

        $("#gridAddEditDialog").attr("title", "Add Task.");
        $("#gridAddEditDialog_save").data("currentaction", "add");
        $("#gridAddEditDialog_txtTaskId").prop("disabled", false);
       // DialogOpen();
        $("#gridAddEditDialog").dialog("open");
        $('#TaskRow').hide();

    });

    $(document).on("click", ".clsEditRow", function (é) {
        if ($(this).css('cursor') == 'not-allowed') {
            return false;
        }
        $("#gridAddEditDialog").attr("title", "Update Task.");
        $("#gridAddEditDialog_save").data("currentaction", "edit");
        var currentTRID = $(this).attr("id").split("_edit")[0];


        $("#gridAddEditDialog").dialog("open");


        $("#gridAddEditDialog_txtTaskId").val($("#" + currentTRID + "_taskid").html());
        $("#TaskRow").show();
        $("#gridAddEditDialog_txtTaskId").prop("disabled", true);
        $("#gridAddEditDialog_txtTaskDescription").val($("#" + currentTRID + "_taskdescription").html());

        $("#gridAddEditDialog_dtStartDate").val($("#" + currentTRID + "_DtePicker_StartDate").html().replace(/\-/g, '/'));
        $("#gridAddEditDialog_dtEndDate").val($("#" + currentTRID + "_DtePicker_EndDate").html().replace(/\-/g, '/'));
        $("#gridAddEditDialog_txtTaskAssignedBy").val($("#" + currentTRID + "_assignby").html());
        $("#gridAddEditDialog_txtTaskAssignedTo").val($("#" + currentTRID + "_assignto").html());
        var StatusCode = $("#" + currentTRID + "_status").html();
        StatusCode = getStatus(StatusCode);
       // alert(StatusCode);
       // $("#gridAddEditDialog_txtTaskCompleted").val($("#" + currentTRID + "_status").html());
       // $('#gridAddEditDialog_txtTaskCompleted option[value="' + StatusCode + '"]').attr("selected", "selected");
        $("#gridAddEditDialog_txtTaskCompleted").val(StatusCode);

    });
    function getStatus(statusCode) {
        if (statusCode == "Started") {
            return 1;
        }
        else if (statusCode == "In-Progress") {
            return 2;
        }
        else if (statusCode == "Review") {
            return 3;
        }
       else if (statusCode == "Completed") {
            return 4;
        }
      

    }

    $(document).on("click", "#gridAddEditDialog_save", function () {

       
        var currentAction = $("#gridAddEditDialog_save").data("currentaction");

        var pTask = {
            Id: $("#gridAddEditDialog_txtTaskId").val(),
            TaskDescription: $("#gridAddEditDialog_txtTaskDescription").val(),
            StartDate: $("#gridAddEditDialog_dtStartDate").val(),
            EndDate: $("#gridAddEditDialog_dtEndDate").val(),
            AssignedBy: $("#gridAddEditDialog_txtTaskAssignedBy").val(),
            AssignedTo: $("#gridAddEditDialog_txtTaskAssignedTo").val(),
            Completion: $("#gridAddEditDialog_txtTaskCompleted").val()
        }
        var dstartdate = new Date(pTask.StartDate);
        var denddate = new Date(pTask.EndDate);
        ValidationRemoving(pTask)

        var currentAccessUrl = "";
        var currentAlertSuccessMsg = "";
        var currentAlertErrorMsg = "";

        if (currentAction == "add") {
            currentAccessUrl = teamMgtURLs.addNewTask;
            currentAlertSuccessMsg = "New Task Created...";
            currentAlertErrorMsg = "Task Not Created...";
        }
        else // --- edit action
        {
            currentAccessUrl = teamMgtURLs.editTask;
            currentAlertSuccessMsg = "Task Updated...";
            currentAlertErrorMsg = "Task Not Updated.";
        }
       
        if ( pTask.TaskDescription == null || pTask.StartDate == null || pTask.EndDate == null || pTask.AssignedBy == null || pTask.AssignedTo == null || pTask.Completion == null) {
            
            ErrorMessageHandling(pTask);

            $('#DialogerrorMessage').removeClass('hidden');
            $('#DialogerrorMessage').css("display", "block");
           
            $('#DialogerrorMessage').text("Please fill all fields...!!!!!!");
            setTimeout(function () {
                $("#DialogerrorMessage").fadeOut("slow", function () {
                    // $("#successMessage").remove();
                    $('#DialogerrorMessage').addClass('hidden');
                   
                    
                });

            }, 2000);

            return false;
        }
        else if (pTask.TaskDescription == "" || pTask.StartDate == "" || pTask.EndDate == "" || pTask.AssignedBy == "" || pTask.AssignedTo == "" || pTask.Completion == "") {
            ErrorMessageHandling(pTask);
           
           
           

            $('#DialogerrorMessage').text("Please fill all fields...!!!!!!");

            $('#DialogerrorMessage').removeClass('hidden');
            $('#DialogerrorMessage').css("display", "block");
            setTimeout(function () {
                $("#DialogerrorMessage").fadeOut("slow", function () {
                    // $("#successMessage").remove();
                    $('#DialogerrorMessage').addClass('hidden');
                    
                });

            }, 2000);
            return false;
        }

        else if (validateDate(pTask.StartDate) != true) {
            dateValidationError(1);
            $('#DialogerrorMessage').removeClass('hidden');
            $('#DialogerrorMessage').css("display", "block");
            $('#DialogerrorMessage').text("Please enter valid start date...!!!!!!");
            setTimeout(function () {
                $("#DialogerrorMessage").fadeOut("slow", function () {
                    // $("#successMessage").remove();
                    $('#DialogerrorMessage').addClass('hidden');
                });

            }, 2000);
            return false;
        }
        else if (validateDate(pTask.EndDate) != true) {
            dateValidationError(0);
            $('#DialogerrorMessage').removeClass('hidden');
            $('#DialogerrorMessage').css("display", "block");
            $('#DialogerrorMessage').text("Please enter valid end date...!!!!!!");
            setTimeout(function () {
                $("#DialogerrorMessage").fadeOut("slow", function () {
                    // $("#successMessage").remove();
                    $('#DialogerrorMessage').addClass('hidden');
                });

            }, 2000);
            return false;
        }
       

    else if (dstartdate.getTime()> denddate.getTime())
        {
        $('#DialogerrorMessage').removeClass('hidden');
        $('#DialogerrorMessage').css("display", "block");
        $('#DialogerrorMessage').text("Startdate will be less than end date...!!!!!!");
        setTimeout(function () {
            $("#DialogerrorMessage").fadeOut("slow", function () {
                // $("#successMessage").remove();
                $('#DialogerrorMessage').addClass('hidden');
            });

        }, 2000);
        return false;
        }
        else {

            $.ajax({
                url: currentAccessUrl,
                type: "POST",
                dataType: "html",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(pTask),
                success: function (result, xhr, settings) {
                    $('#TaskManagementDataTable').dataTable().fnDestroy();
                    $("#gridRowForTeamManagement").html("");
                    $("#gridRowForTeamManagement").html(result);
                    $('#TaskManagementDataTable').DataTable({ "aaSorting": [], "aoColumnDefs": [{ "bSortable": false, "aTargets": [7] }] });
                    $("#gridAddEditDialog").dialog('close');
                   
                    $('#successMessage').removeClass('hidden');
                    $('#successMessage').css("display", "block");
                    $('#successMessage').text(currentAlertSuccessMsg);
                    setTimeout(function () {
                        $("#successMessage").fadeOut("slow", function () {
                           // $("#successMessage").remove();
                            $('#successMessage').addClass('hidden');
                        });

                    }, 3000);
                },
                error: function (xhr, status, error) {
                    alert(currentAlertErrorMsg)
                }

            });
        }

        });
    

});
function ErrorMessageHandling(pTask) {
    if (pTask.TaskDescription == "") {
        $('#gridAddEditDialog_txtTaskDescription').addClass('divBorder');
    }
    if (pTask.AssignedBy == "") {
        $('#gridAddEditDialog_txtTaskAssignedBy').addClass('divBorder');
    }
    if (pTask.AssignedTo == "") {
        $('#gridAddEditDialog_txtTaskAssignedTo').addClass('divBorder');
    }
    if (pTask.StartDate == "") {
        $('#gridAddEditDialog_dtStartDate').addClass('divBorder');
    }
    if (pTask.EndDate == "") {
        $('#gridAddEditDialog_dtEndDate').addClass('divBorder');
    }
    if (pTask.Completion == "") {
        $('#gridAddEditDialog_txtTaskCompleted').addClass('divBorder');
    }
}

function dateValidationError(datestatus) {
   
    if (datestatus == 1) {
        
            $('#gridAddEditDialog_dtStartDate').addClass('divBorder');
      
    }
    else
        {
        $('#gridAddEditDialog_dtEndDate').addClass('divBorder');
    }
    
}

function ValidationRemoving(pTask) {
   
    $('#gridAddEditDialog_txtTaskDescription').removeClass('divBorder');
   
        $('#gridAddEditDialog_txtTaskAssignedBy').removeClass('divBorder');
    
        $('#gridAddEditDialog_txtTaskAssignedTo').removeClass('divBorder');
   
        $('#gridAddEditDialog_dtStartDate').removeClass('divBorder');
   
        $('#gridAddEditDialog_dtEndDate').removeClass('divBorder');
   
        $('#gridAddEditDialog_txtTaskCompleted').removeClass('divBorder');
    
}

function clearDialogElements()
{
    
    $("#gridAddEditDialog_txtTaskId").val("");
    $("#gridAddEditDialog_txtTaskId").val("");
    $("#gridAddEditDialog_txtTaskDescription").val("");

    $("#gridAddEditDialog_dtStartDate").val("");
    $("#gridAddEditDialog_dtEndDate").val("");
    //$("#gridAddEditDialog_txtTaskAssignedBy").val("");
   // $("#gridAddEditDialog_txtTaskAssignedTo").val("");
    $("#gridAddEditDialog_txtTaskCompleted").val("");
}

$(document).on('click', '.clsDeleteTask', function () {
    if ($(this).css('cursor') == 'not-allowed') {
        return false;
    }

    var TaskId = $(this).attr("id").split("_row_delete")[0];
    var confirmationStatus = confirm("Are you sure want to delete the task id - " + TaskId);

    if (confirmationStatus == true) {

        $.ajax({
            url: teamMgtURLs.deleteTask + "/" + TaskId,
            type: "GET",
            dataType: 'html',
            success: function (result, xhr, settings) {
                if ($("#TaskManagementDataTable"))
                {
                    $('#TaskManagementDataTable').dataTable().fnDestroy();
                    $("#gridRowForTeamManagement").html("");
                    $("#gridRowForTeamManagement").html(result);
                    $('#TaskManagementDataTable').DataTable({ "aaSorting": [], "aoColumnDefs": [{ "bSortable": false, "aTargets": [7] }] });
                }
                },
            error: function (xhr, status, error) {
                alert('Task not deleted.');
            }

        });

    }

});

$(document).on("change", "#selUser", function () {

    if ($(this).css('cursor') == 'not-allowed') {
        return false;
    }
    var username = $(".my_select_box").chosen().val();

    $.ajax({
        url: teamMgtURLs.SearchUserTask + "/" + username,
        type: "GET",
        data: { username: username },
        dataType: 'html',
        success: function (result, xhr, settings) {
            if ($("#TaskManagementDataTable")) {
                $('#TaskManagementDataTable').dataTable().fnDestroy();
                $("#gridRowForTeamManagement").html("");
                $("#gridRowForTeamManagement").html(result);
                $('#TaskManagementDataTable').DataTable({ "aaSorting": [], "aoColumnDefs": [{ "bSortable": false, "aTargets": [7] }] });
            }
        },
        error: function (xhr, status, error) {
            alert('Search Error.');
        }

    });
});
function TimeOutMessageHandling() {
    setTimeout(function () {
        $("#DialogerrorMessage").fadeOut("slow", function () {
           
           // $('#DialogerrorMessage').addClass('hidden');


        });

    }, 2000);
}



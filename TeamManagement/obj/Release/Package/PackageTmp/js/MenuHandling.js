﻿$(document).ready(
  /* This is the function that will get executed after the DOM is fully loaded */
  function () {



      // Create a jqxMenu
      $("#jqxMenu").jqxMenu({
          width: '310',
          height: '30',
          mode: 'horizontal',
          theme: 'orange',
          showTopLevelArrows: true
      });
      // Set up the open directions.
      $("#jqxMenu").jqxMenu('setItemOpenDirection', 'Services', 'left', 'up');
      $("#jqxMenu").jqxMenu('setItemOpenDirection', 'ContactUs', 'left', 'down');
      $("#jqxMenu").css('visibility', 'visible');
  });


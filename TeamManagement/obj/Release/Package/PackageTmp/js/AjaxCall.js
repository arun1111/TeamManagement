﻿
function AjaxCall(strUrl, dataSource, datatype) {

    var returnData;
    if (datatype == 1) {
        jQuery.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: strUrl,
            processData: false,
            async: false,
            cache: false,
            data: JSON.stringify(dataSource),
            dataType: "text",
            success: function (data) {
                returnData = data
            },
            error: function (data) {
                //alert(data)
            }
        });
    }
    else if (datatype == 2) {
        jQuery.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: strUrl,
            processData: false,
            async: false,
            data: JSON.stringify(dataSource),
            dataType: "json",
            success: function (data) {
                returnData = data
            },
            error: function (data) {
                alert(data)
            }
        });
    }
    else if (datatype == 3) {
        jQuery.ajax({
            url: strUrl,
            type: "POST",
            processData: false,
            contentType: "text/xml",
            async: false,
            data: dataSource,
            success: function (data) {
                returnData = data
            },
            error: function (data) {
                // alert(data)
            }
        });

    }
    else if (datatype == 4) {
        jQuery.ajax({
            url: strUrl,
            type: "POST",
            processData: false,
            contentType: "text/xml",
            async: false,
            data: null,
            success: function (data) {
                returnData = data
            }
            ,
            error: function (data) {
                //alert(data)
            }
        });

    }
    else if (datatype == 5) {
        jQuery.ajax({
            url: strUrl,
            type: "POST",
            processData: false,
            dataType: 'html',
            async: false,
            data: null,
            success: function (data) {
                returnData = data
            }
            ,
            error: function (data) {
                //alert(data)
            }
        });
    }
    else if (datatype == 6) {
        jQuery.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: strUrl,
            processData: false,
            async: false,
            data: JSON.stringify(dataSource),
            dataType: 'html',
            success: function (data) {
                returnData = data
            },
            error: function (data) {
                alert(data)
            }
        });
    }

    return returnData;
}



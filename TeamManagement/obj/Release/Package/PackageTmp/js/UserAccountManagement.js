﻿
function AccountCreation(username, email, password,Firstname,Lastname) {

    $.ajax({
        type: "POST",
        url: UserRegistrationUrl,

        data: { username: username, email: email, password: password,Firstname:Firstname,Lastname:Lastname },
        dataType: "json",
        success: function (data, status, jqXHR) { SuccessFun(data, status, jqXHR) },
        processdata: true,
        error: function (xhr) {
            errorFunc(xhr);
        }
    });
}

function ErrorMessages(txtmessage)
{
    $('#DialogLoginerrorMessage').removeClass('hidden');
    $('#DialogLoginerrorMessage').css("display", "block");
    $('#DialogLoginerrorMessage').text(txtmessage);
    setTimeout(function () {
        $("#DialogLoginerrorMessage").fadeOut("slow", function () {
            // $("#successMessage").remove();

            $('#DialogLoginerrorMessage').addClass('hidden');

        });

    }, 3000);
    $('#Loginusername').addClass('divBorder');
    $('#Loginpassword').addClass('divBorder');
}
function SuccessFun(data, status, JqXHR) {
    
    if (data.ValidationStatus == 1) {
        alert("Registered Successfully. Please login with your new credentials")
        window.location = data.url;

    }
    else if (data.ValidationStatus == 2) {
      
        ErrorMessages("Email ID already exists!! Please try with some other Email ID");
    }
    else {
        ErrorMessages("Username already exists!! Please try with some other usernames");
        
    }
}
function errorFunc() {
    alert("Error in request!!")

}

$(function () {

    $('#login-form-link').click(function (e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        // e.preventDefault();
    });
    $('#register-form-link').click(function (e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        //e.preventDefault();
    });
    // ------------------------------All Registration submission activities---------------------------------------------------------
    $("#register-submit").click(function () {

        var Firstname = $("#Firstname").val();
        var Lastname = $("#Lastname").val();
        var username = $("#Registerusername").val();
        var email = $("#Registeremail").val();
        var password = $("#Registerpassword").val();
        var confirmpassword = $("#Registerconfirmpassword").val();

        if (Lastname == null || username == null || Firstname == null || email == null || password == null || confirmpassword == null) {
            ErrorMessages("Please fill all fields...!!!!!!");
            return false;
        }
        else if (Lastname == "" || username == "" || Firstname == "" || email == "" || password == "" || confirmpassword == "") {
            ErrorMessages("Please fill all fields...!!!!!!");
            return false;
        }
        
         else if (!username.match(/^\d*[a-zA-Z]/g)){
             ErrorMessages("Username sholud contain atleast one  alphabet");
            return false;
        }

        else if ((password.length) < 8) {
            ErrorMessages("Password should atleast 8 character in length...!!!!!!");
            return false;
        } else if (password !== confirmpassword) {
            ErrorMessages("Your passwords don't match. Try again?");
            return false;
        }

        else if (validateEmail(email) != true) {
            ErrorMessages("Your email is not in a correct format. Try again?");
        }

        else {

            AccountCreation(username, email, password,Firstname,Lastname);
        }
    });
});
$(document).ready(function () {
    //menu hide 
    $("#divMenu").hide();
    $(".logoutcls").hide();
    $("#Registerusername").focusout(function () {
        var lastChar = this.value.substr(this.value.length - 1);
        if (lastChar == '.') {
            $("#Registerusername").val(this.value.slice(0, -1));
        }
    });
    $("#Registerusername").keypress(function (e) {

        var regex = new RegExp("^[a-zA-Z0-9.]");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (event.keyCode != 8) {
          
            if (regex.test(str)) {
                if (this.value.length == 0 && e.charCode == 46) {
                    e.preventDefault();
                    return false;
                }
                if (e.which == 46) {
                    var lastChar = this.value.substr(this.value.length - 1);

                    if (lastChar == '.') {

                        e.preventDefault();
                        return false;
                    }

                }
                return true;
            }

            e.preventDefault();
            return false;
        }
    });

});
// ------------------------------All Login  activities---------------------------------------------------------

$(document).ready(function () {
    $("#Loginusername").focusout(function () {
        var lastChar = this.value.substr(this.value.length - 1);
        if (lastChar == '.') {
            $("#Loginusername").val(this.value.slice(0, -1));
        }
    });
    $("#Loginusername").keypress(function (e) {
        if (event.keyCode != 8) {


            var regex = new RegExp("^[a-zA-Z0-9.]");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

            if (regex.test(str)) {
                if (this.value.length == 0 && e.charCode == 46) {
                    e.preventDefault();
                    return false;
                }
                if (e.which == 46) {
                    var lastChar = this.value.substr(this.value.length - 1);

                    if (lastChar == '.') {

                        e.preventDefault();
                        return false;
                    }

                }
                return true;
            }

            e.preventDefault();
            return false;
        }
    });
    //firstname validation 
    $("#Firstname").keypress(function (e) {

        if (event.keyCode != 8) {


            var regex = new RegExp("^[a-zA-Z]");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

            if (regex.test(str)) {
                if (this.value.length == 0 && e.charCode == 46) {
                    e.preventDefault();
                    return false;
                }
                if (e.which == 46) {
                    var lastChar = this.value.substr(this.value.length - 1);

                    if (lastChar == '.') {

                        e.preventDefault();
                        return false;
                    }

                }
                return true;
            }

            e.preventDefault();
            return false;
        }
    });
    //lastname validation 
    //firstname validation 
    $("#Lastname").keypress(function (e) {
        if (event.keyCode != 8) {


            var regex = new RegExp("^[a-zA-Z]");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

            if (regex.test(str)) {
                if (this.value.length == 0 && e.charCode == 46) {
                    e.preventDefault();
                    return false;
                }
                if (e.which == 46) {
                    var lastChar = this.value.substr(this.value.length - 1);

                    if (lastChar == '.') {

                        e.preventDefault();
                        return false;
                    }

                }
                return true;
            }

            e.preventDefault();
            return false;
        }
    });
    $(document).on("click", "#loginsubmit", function (e) {
        //$('#loginusername').removeClass('divBorder');
        //$('#Loginpassword').removeClass('divBorder');
        //debugger;
        var username = $("#Loginusername").val();
        var password = $("#Loginpassword").val();
        if ((username == null || username == "") && (password == null || password == "")) {
            e.stopPropagation();
            $('#DialogLoginerrorMessage').removeClass('hidden');
            $('#DialogLoginerrorMessage').css("display", "block");
            $('#DialogLoginerrorMessage').text("Please enter Username and Password");
            setTimeout(function () {
                $("#DialogLoginerrorMessage").fadeOut("slow", function () {
                    // $("#successMessage").remove();
                    
                    $('#DialogLoginerrorMessage').addClass('hidden');
                    
                });
               
            }, 1000);
            $('#Loginusername').addClass('divBorder');
            $('#Loginpassword').addClass('divBorder');
            
        }
        else if (username == null || username == "") {
           
            $('#DialogLoginerrorMessage').removeClass('hidden');
            $('#DialogLoginerrorMessage').css("display", "block");
            $('#DialogLoginerrorMessage').text("Please enter Username");
            setTimeout(function () {
                $("#DialogLoginerrorMessage").fadeOut("slow", function () {
                    // $("#successMessage").remove();
                    $('#DialogLoginerrorMessage').addClass('hidden');
                });

            }, 3000);
            $('#Loginusername').addClass('divBorder');
        }
        else if (password == null || password == "") {
            $('#DialogLoginerrorMessage').removeClass('hidden');
            $('#DialogLoginerrorMessage').css("display", "block");
            $('#DialogLoginerrorMessage').text("Please enter Password");
            setTimeout(function (ev) {
                $("#DialogLoginerrorMessage").fadeOut("slow", function () {
                    // $("#successMessage").remove();
                    $('#DialogLoginerrorMessage').addClass('hidden');
                });

            }, 3000);
            $('#Loginpassword').addClass('divBorder');
        }
        else {
            var LoginValidationStatus = LoginValidation(username, password);
        }
        e.stopPropagation();
    });
   
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(email);
}
//--------------------------check the user is a valid user or not
function LoginValidation(username, password) {
    $.ajax({
        type: "POST",
        url: UserValidation,

        data: { username: username, password: password },
        dataType: "json",
        success: function (data, status, jqXHR) { SuccessValidationFun(data, status, jqXHR) },
        processdata: true,
        error: function (xhr) {
            errorValidationFunc(xhr);
        }
    });
}
function SuccessValidationFun(data, status, jqXHR) {
    if (data.ValidationStatus == 1) {
        window.location = data.url;
    }
    else {
        $('#DialogLoginerrorMessage').removeClass('hidden');
        $('#DialogLoginerrorMessage').css("display", "block");
        $('#DialogLoginerrorMessage').text("Username or password is incorrect");
        setTimeout(function (ev) {
            $("#DialogLoginerrorMessage").fadeOut("slow", function () {
                // $("#successMessage").remove();
                $('#DialogLoginerrorMessage').addClass('hidden');
            });

        }, 3000);
        $('#Loginusername').addClass('divBorder');
        $('#Loginpassword').addClass('divBorder');
    }
}
function errorValidationFunc() {
    $('#DialogLoginerrorMessage').removeClass('hidden');
    $('#DialogLoginerrorMessage').css("display", "block");
    $('#DialogLoginerrorMessage').text("Error in request");
    setTimeout(function (ev) {
        $("#DialogLoginerrorMessage").fadeOut("slow", function () {
            // $("#successMessage").remove();
            $('#DialogLoginerrorMessage').addClass('hidden');
        });

    }, 3000);
}
﻿var teamPageUris = {
    addTeam: '../InternalTeamManagemet/AddTeam',
    addMemebr: '../InternalTeamManagemet/AddTeamMember',
    teamRename: '../InternalTeamManagemet/TeamRename',
    deleteMember: '../InternalTeamManagemet/DeleteMember',
    deleteTeam: '../InternalTeamManagemet/DeleteTeam'
};

var teamTreeSelectedNode;
var wWidth = $(window).width();
var dWidth = wWidth * 0.4;
var wHeight = $(window).height();
var dHeight = wHeight * 0.4;
var divEditTeamLead = $("#ReplaceTeamLeader").val();

function InitializeTree() {
    $("#teamstructure").jstree({
        "core": {
            "multiple": false,
            "animation": 0,
            "check_callback": true
        }
        ,
        "plugins": ["themes", "json_data", "crrm"]
    });
}

$(document).ready(function () {
   
    $("#divMenu").show();
    //choosen dropdown for teamlead selection 
   
    if (UserID == 1) {
        $("#teampageactions").show();
    }
    else {
        $("#teampageactions").hide();
    }
    //---------------------------------------------------
    $("#ReplaceTeamLeader").hide();
    $("#deleteteam").hide();
    $("#assignmember").hide();
    $("#changeteamlead").hide();
    $("#removemember").hide();
    $('#renameteam').hide();
    $('#renameteam_dialog').show();
    $('#addteam_dialog').hide();
    $('#assignmember_dialog').hide();
   // $('#ReplaceTeamLeader').hide();
    

    InitializeTree();

    $("#renameteam_dialog").dialog(
            {

                width: dWidth,
                autoOpen: false,
                height: dHeight,
                cache: false,
                modal: true,
                open: function () {
                },
                close: function () {
                    $('#renameteam_dialog').data('currentteamid', '');
                    $('#renameteam_dialog').data('currentteamname', '');
                    $('#renameteam_dialog_value').val('');
                    $('#renameteam_dialog').dialog('close');

                }
            });

    $('#renameteam_dialog').dialog('close');

    $("#addteam_dialog").dialog(
        {

            width: dWidth,
            autoOpen: false,
            height: dHeight,
            cache: false,
            modal: true,
            open: function () {
                PopulateDropdown("1","teamleadname","CreateTeam");
                
            },
            close: function () {
                $('#teamleadname', this).chosen('destroy');
                $('#addteam_dialog').dialog('close');

            }
        });
   
   
    $('#addteam_dialog').dialog('close');

    $("#assignmember_dialog").dialog(
       {

           width: dWidth,
           autoOpen: false,
           height: dHeight,
           cache: false,
           modal: true,
           open: function () {
               PopulateDropdown("1", "membername","AssignMemeber");
           },
           close: function () {
               $('#assignmember_dialog').data('currentteamid', '');
               $('#assignmember_dialog').data('currentleadid', '');
               $('#membername').chosen().val('').trigger('chosen:updated');
               $('#addteam_dialog').dialog('close');

           }
       });

    $('#assignmember_dialog').dialog('close');

    $('#teamstructure').on("select_node.jstree", function (e, data) {
        teamTreeSelectedNode = data;
    });

});

$(document).on('click', '.teamname .jstree-ocl', function (e) {
    e.stopImmediatePropagation();
});

$(document).on('click', '.teamname', function (e) {

    $('#renameteam').show();
    $("#deleteteam").show();
    $("#assignmember").show();
    $("#changeteamlead").hide();
    $("#removemember").hide();
    e.preventDefault();

});

$(document).on('click', '.teamlead .jstree-ocl', function (e) {
    e.stopImmediatePropagation();
});

$(document).on('click', '.teamlead', function (e) {
    $('#renameteam').hide();
    $("#deleteteam").hide();
    $("#assignmember").show();
    $("#changeteamlead").show();
    $("#removemember").hide();
    e.stopImmediatePropagation();

});

$(document).on('click', '.teammumbers .jstree-ocl', function (e) {
    e.stopImmediatePropagation();
});

$(document).on('click', '.teammumbers', function (e) {
    $('#renameteam').hide();
    $("#deleteteam").hide();
    $("#assignmember").hide();
    $("#changeteamlead").hide();
    $("#removemember").show();
    e.stopImmediatePropagation();

});

$(document).on('click', '#renameteam', function () {

    $('#renameteam_dialog').data('currentteamid', teamTreeSelectedNode.node.data.teamid);
    $('#renameteam_dialog').data('currentteamname', teamTreeSelectedNode.node.text.trim());
    $('#renameteam_dialog_value').val(teamTreeSelectedNode.node.text.trim());
    $('#renameteam_dialog').dialog('open');
});

$(document).on('click', '#renameteam_dialog_save', function (e) {
    if (teamTreeSelectedNode.node.text.trim() == $('#renameteam_dialog_value').val())
        alert('no value changes');

    else {
        var pTeamId = teamTreeSelectedNode.node.data.teamid;
        var pTeamName = $('#renameteam_dialog_value').val();


        $.ajax({
            url: teamPageUris.teamRename + "/" + pTeamId + "/" + pTeamName,
            type: "GET",
            success: function (result, xhr, settings) {
                var instance = $("#teamstructure").jstree(true);
                instance.rename_node(teamTreeSelectedNode.node.id, $('#renameteam_dialog_value').val());
                $('#renameteam_dialog').dialog('close');
            },
            error: function (xhr, status, error) {
                alert('Rename not success.');
            }

        });

    }

});

$(document).on('click', '#removemember', function () {
    var confirmationStatus = confirm("Are you sure remove member - " + teamTreeSelectedNode.node.text.trim());

    if (confirmationStatus == true) {
        var pTeamId = $("#" + teamTreeSelectedNode.node.parents[1]).data("teamid");
        var pMemberId = teamTreeSelectedNode.node.data.teammemberid;

        $.ajax({
            url: teamPageUris.deleteMember + "/" + pTeamId + "/" + pMemberId,
            type: "GET",
            success: function (result, xhr, settings) {
                var instance = $("#teamstructure").jstree(true);
                instance.delete_node(instance.get_selected());
            },
            error: function (xhr, status, error) {
                alert('Delete member not success.');
            }

        });

    }

});

$(document).on('click', '#deleteteam', function () {
    var confirmationStatus = confirm("Are you sure want to remove team - " + teamTreeSelectedNode.node.text.trim());

    if (confirmationStatus == true) {

        var pTeamId = teamTreeSelectedNode.node.data.teamid;

        $.ajax({
            url: teamPageUris.deleteTeam + "/" + pTeamId,
            type: "GET",
            success: function (result, xhr, settings) {
                var instance = $("#teamstructure").jstree(true);
                instance.delete_node(instance.get_selected());
            },
            error: function (xhr, status, error) {
                alert('Delete team not success.');
            }

        });
    }
});
//Add a new team
$(document).on('click', '#addteam', function () {

    $('#addteam_dialog').dialog('open');

});

$(document).on('click', '#btnReplaceteamLead', function () {
    var teamleadId = teamTreeSelectedNode.node.data.teamleadid;

    $("#ReplaceTeamLeader").dialog(
      {

          width: dWidth,
          autoOpen: false,
          height: dHeight,
          cache: false,
          modal: true,
          open: function () {
             
              PopulateDropdown("1", "drpReplaceTeamLead", "TeamLeadSelect");
              $("#drpReplaceTeamLead option[value="+teamleadId+"]").remove();
              $('#drpReplaceTeamLead').trigger("chosen:updated");
              
          },
          close: function () {
              $('#drpReplaceTeamLead', this).chosen('destroy');
              $('#ReplaceTeamLeader').dialog('close');

          }
      });

    
    $("#ReplaceTeamLeader").dialog('open');



});

$(document).on('click', '#TeamLeaderChange_save', function () {
    var pTeamId = $("#" + teamTreeSelectedNode.node.parents[0]).data("teamid");
    var NewteamleadId = $("#drpReplaceTeamLead").val();
   
    var returnData = AjaxCall(urlReplaceTeamLead, { Purpose: "ReplaceTeamLead", TeamID: pTeamId, NewTeamLeadID: NewteamleadId },6);
    $("#teamstructure").jstree(true).destroy();
    $("#teamstructure").html("");
    $("#teamstructure").html(returnData);
    InitializeTree();
    //$("#ReplaceTeamLeader").val(divEditTeamLead)
    $('#ReplaceTeamLeader').dialog('close');
    location.reload();


});

$(document).on('click', '#addteam_dialog_save', function () {
    var pTeamName = $('#addteam_dialog').find("#teamname").val();
    var pTeamLeadId = $('#teamleadname').val(); 


    $.ajax({
        url: teamPageUris.addTeam + "/" + pTeamName + "/" + pTeamLeadId,
        type: "GET",
        dataType: 'html',
        success: function (result, xhr, settings) {
            $("#teamstructure").jstree(true).destroy();
            $("#teamstructure").html("");
            $("#teamstructure").html(result);
            InitializeTree();
            $('#addteam_dialog').dialog('close');
        },
        error: function (xhr, status, error) {
            alert('team not created.');
        }

    });

});

$(document).on('click', '#assignmember', function () {
    var instance = $("#teamstructure").jstree(true);
    var teamId = $('#' + instance.get_selected()).closest('.teamname').data().teamid;
    var teamleadId = $('#' + instance.get_selected()).data().teamleadid;

    $('#assignmember_dialog').data('currentteamid', teamId);
    $('#assignmember_dialog').data('currentleadid', teamleadId);

    $('#assignmember_dialog').dialog('open');

});

$(document).on('click', '#assignmember_dialog_save', function () {

    var pTeamId = $('#assignmember_dialog').data().currentteamid;
    //$('#assignmember_dialog').find('#membername').val();
    var pMemberId = [];
    var pMemberIds = "";
  
         pMemberId = $('#membername').chosen().val();
         pMemberIds = pMemberId.toString();
 
    $.ajax({
        url: teamPageUris.addMemebr,
        type: "POST",
        dataType: 'html',
        data:{pTeamId:pTeamId,pMemberIds:pMemberIds},
        success: function (result, xhr, settings) {
            $("#teamstructure").jstree(true).destroy();
            $("#teamstructure").html("");
            $("#teamstructure").html(result);
            InitializeTree();
            $('#assignmember_dialog').dialog('close');
        },
        error: function (xhr, status, error) {
            alert('member not created.');
        }

    });

});

function PopulateDropdown(MultipleOrsingle, ID, Purpose) {
    var TeamID = 0;
    if (Purpose != "AssignMemeber") {
        TeamID = -1;
    }
    else {
        var instance = $("#teamstructure").jstree(true);
        TeamID = $('#' + instance.get_selected()).closest('.teamname').data().teamid;
    }
    var returnData = AjaxCall(urlGetDropdownDetails, { Purpose: "TeamleadName", TeamID: TeamID }, 2);
    $('#' + ID + '').find('option').remove();
    for (var key in returnData) {
        if (returnData.hasOwnProperty(key)) {

            $('#' + ID + '').append('<option value="' + returnData[key].MemberID + '">' + returnData[key].MemberName + '</option>');
        }
    }

    $('#' + ID + '').chosen({
        disable_search_threshold: 5,

        no_results_text: "Oops, nothing found!",

    })


}
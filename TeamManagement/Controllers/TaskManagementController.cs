﻿using System.Collections.Generic;
using System.Web.Mvc;
using TeamManagement.Models;
using DbIntegrationOracle;
using System;
using TeamManagement;
using System.Data;


namespace BCBSTeamManagement.Controllers
{
    [SessionTimeOut]
    public class TaskManagementController : Controller
    {
        Integration objIng = new Integration();
        TaskManagementModel objTmnt = new TaskManagementModel();
        [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult TaskManagement()
        {
            if (Convert.ToInt32(Session["UserSessionValid"]) == 1)

            {
                TeamUserDetails();
                return View(objTmnt); }
            else
            {
                
                return RedirectToAction("Login", "Login");

            }
        }
        public void TeamUserDetails()
        {
            objTmnt.UserID = Convert.ToInt32(Session["UserID"]);

            DataTable dt = objIng.GetTeamDetails(objTmnt.UserID);
            if (dt.Rows.Count>0)
            {
                objTmnt.TeamId = Convert.ToInt32(dt.Rows[0]["TeamId"]);
                objTmnt.TeamLeadId = Convert.ToInt32(dt.Rows[0]["TeamLeadId"]);
                Session["TeamLeadId"] = objTmnt.TeamLeadId;
                objTmnt.TeamName = dt.Rows[0]["TeamName"].ToString();
                objTmnt.TeamLeadName = dt.Rows[0]["TeamLeadName"].ToString();
            }
            else
            {
                objTmnt.TeamId = 0;
                objTmnt.TeamLeadId = 0;
                Session["TeamLeadId"] = 0;
                objTmnt.TeamName = "Not Assigned";
                objTmnt.TeamLeadName = "Not Assigned";

            }
            List<AssignedToAssignedByDetails> UserListDb = objIng.GetAssignedToDropdownDetails(objTmnt.TeamId, objTmnt.TeamLeadName);
            List<AssignedToAssignedByModel> UserList = new List<AssignedToAssignedByModel>();
            if (UserListDb != null)
            {
                foreach (AssignedToAssignedByDetails item in UserListDb)
                {
                    UserList.Add(new AssignedToAssignedByModel
                    {
                        FullName = item.FullName,
                        UserName = item.UserName
                    });
                }
            }
            objTmnt.UserDropDownList = UserList;
        }
        public JsonResult GetTaskDetails()
        {
            TaskManagementModel tobj = new TaskManagementModel();
            List<TaskManagement> parts = new List<TaskManagement>();
          
            return Json("");
        }
        [HttpPost]
        public PartialViewResult AddNewTask(TaskManagement pTask)
        {
            objTmnt.TaskDetailist = objTmnt.AddTaskDetail(pTask);
            var viewResult = PartialView("_GridRowForTaskManagement", objTmnt);
            return viewResult;

        }
        [HttpPost]
        public PartialViewResult EditTask(TaskManagement pTask)
        {
            objTmnt.EditTaskDetails(pTask);
            objTmnt.TaskDetailist = objTmnt.GetTasks();
            var viewResult = PartialView("_GridRowForTaskManagement", objTmnt);
             return viewResult;
            
        }

        [HttpGet]
        public PartialViewResult DeleteTask(int pTaskId)
        {
             objTmnt.DeleteTask(pTaskId);
            objTmnt.TaskDetailist = objTmnt.GetTasks();
            var viewResult = PartialView("_GridRowForTaskManagement", objTmnt);
            return viewResult;


        }
        [HttpGet]
        public PartialViewResult SearchTask(string username)   //Function to search task of particular user
        {

            objTmnt.TaskDetailist.Clear();
            if (username != "All")
            {
                List<TaskManagementDetails> Task = objIng.GetAllTasksForSelectedUser(username);

                if (Task != null)
                {
                    foreach (TaskManagementDetails item in Task)
                    {
                        objTmnt.TaskDetailist.Add(new TaskManagement
                        {
                            Id = Convert.ToInt32(item.Id),
                            TaskDescription = item.TaskDescription,
                            strStartDate = item.StartDate.Value.ToString("MM-dd-yyyy"),
                            strEndDate = item.EndDate.Value.ToString("MM-dd-yyyy"),
                            AssignedBy = item.AssignedBy,
                            AssignedTo = item.AssignedTo,
                            Status = item.Status,
                            Completion = item.Completion,

                        });
                    }
                }
            }
            else
            {
                objTmnt.TaskDetailist = objTmnt.GetTasks();
            }

            var viewResult = PartialView("_GridRowForTaskManagement", objTmnt);
            return viewResult;

        }

        public int Sonarerrorcheck()
        {
            int r =checkStatus()
    ?Convert.ToInt32(1)
    : Convert.ToInt32(1);

            return r;
        }
        public bool checkStatus()
        {
            return true;
        }
    }
}
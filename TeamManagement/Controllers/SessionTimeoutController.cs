﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TeamManagement.Controllers
{
    public class SessionTimeoutController : Controller
    {
        // GET: SessionTimeout
        public ActionResult SessionTimeout()
        {
            Session["UserSessionValid"] = 0;
            return View();
        }
    }
}
﻿using System.Web.Mvc;
using TeamManagement.Models;
using DbIntegrationOracle;
using System.Collections.Generic;

namespace BCBSTeamManagement.Controllers
{
    
    public class LoginController : Controller
    {
        //Object creation of model
        LoginModel objloginmdl = new LoginModel();
        LoginUserValues objLog = new LoginUserValues();
        Integration objEI = new Integration();
        //Login form controller
        public ViewResult Login()
        {
            Session["UserSessionValid"] = 0;
               return View(objloginmdl);
        }
        
        //Validating the user
        public ActionResult UserLoginValidation(string username, string password)
        {
            IRepository ObjRepositary;
            ObjRepositary = new Repository();
            // user is valid then redirect to task management page
            objloginmdl.ValidationStatus = ObjRepositary.Login(username, password); ;
                
                

            if (objloginmdl.ValidationStatus == 1)
            {

                List<LoginUserDetails> list = objEI.GetUserDetails(username);
                if (list != null)
                {
                    foreach (LoginUserDetails item in list)
                    {
                        objLog.FullName = item.FullName;
                        objLog.UserId = item.UserId;
                    }
                }
                Session["UserName"] = username;
                Session["UserID"] = objLog.UserId;
                Session["UserFullName"] = objLog.FullName;
                Session["UserSessionValid"] = 1; 
                return Json(new { ValidationStatus = objloginmdl.ValidationStatus, url = Url.Action("TaskManagement", "TaskManagement") });

            }
            //for non valid users response 0 as validation status to ajax request.
            else {
                return Json(objloginmdl.ValidationStatus, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UserRegistration(string username, string email, string password,string Firstname,string Lastname)
        {
            // user is valid then redirect to task management page
            objloginmdl.ValidationStatus = objEI.Register(username, email, password,Firstname,Lastname);
           
            if (objloginmdl.ValidationStatus == 1)
            {
                return Json(new { ValidationStatus = objloginmdl.ValidationStatus, url = Url.Action("Login", "Login") });

            }

            //for non valid users response 0 as validation status to ajax request.
            else {
                return Json(new { ValidationStatus = objloginmdl.ValidationStatus, JsonRequestBehavior.AllowGet });
            }
        }

    }
}
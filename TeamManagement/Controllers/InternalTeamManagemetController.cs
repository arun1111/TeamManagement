﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeamManagement.Models.Team_Page;
using DbIntegrationOracle;
using BusinessEntity;

namespace TeamManagement.Controllers
{
    public class InternalTeamManagemetController : Controller
    {
       
        MemberDetailsGet objMemDetails = new MemberDetailsGet();
        MemberDetailsGet objmemgetmdl = new MemberDetailsGet();
       
        public ActionResult GetDetails()
        {
            
            var teamStructure = new TeamStructure();
            teamStructure.TeamTreeStructure = teamStructure.GetTeamDetails();
            return View(teamStructure);
        }
        
        public ActionResult AddTeam(string pTeamName,decimal pTeamLeadId)
        {
            var teamStructure = new TeamStructure();
            teamStructure.AddTeam(pTeamName, pTeamLeadId);
            teamStructure.TeamTreeStructure = teamStructure.GetTeamDetails();

            return PartialView("_TeamStructure", teamStructure);
        }
        [HttpPost]
        public ActionResult AddTeamMember(decimal pTeamId,string pMemberIds)
        {
            var teamStructure = new TeamStructure();
            teamStructure.AddMember(pTeamId, pMemberIds);
            teamStructure.TeamTreeStructure = teamStructure.GetTeamDetails();

            return PartialView("_TeamStructure", teamStructure);
        }

        public void TeamRename(decimal pTeamId,string pTeamName)
        {
            new TeamStructure().TeamRename(pTeamId, pTeamName);
        }

        public void DeleteMember(decimal pTeamId,decimal pMemberId)
        {
            new TeamStructure().DeleteMember(pTeamId, pMemberId);
        }

        public void DeleteTeam(decimal pTeamId)
        {
            new TeamStructure().DeleteTeam(pTeamId);
        }
        public JsonResult GetMemberDetails(string Purpose,int TeamID)
        {
            
            IRepository ObjRepositary;
            ObjRepositary = new Repository();
            objmemgetmdl.lstgetMemDetails= ObjRepositary.GetMemeberInformations(TeamID);
            return Json(objmemgetmdl.lstgetMemDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReplaceTeamLeader(string Purpose, int TeamID,int NewTeamLeadID)
        {
            var teamStructure = new TeamStructure();
            IRepository ObjRepositary;
            ObjRepositary = new Repository();
            objmemgetmdl.SaveStatus = ObjRepositary.ReplaceTeamLeader(Purpose,TeamID,NewTeamLeadID);
            if (objmemgetmdl.SaveStatus == 1)
            {
                teamStructure.TeamTreeStructure = teamStructure.GetTeamDetails();

                return PartialView("_TeamStructure", teamStructure);
            }
            else
            {
                return Json(objmemgetmdl.SaveStatus, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
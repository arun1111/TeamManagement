﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;

namespace TeamManagement
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
    public class SessionTimeOutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext context = HttpContext.Current;

            // check if session supported
            if (context.Session != null)
            {
                if (context.Session["UserSessionValid"] == null)
                {
                   // context.Response.Clear();
                  //  context.Response.Redirect("~/SessionManagement/SessionManagement");
                    filterContext.Result = new RedirectToRouteResult(
            new RouteValueDictionary {{ "Controller", "SessionManagement" },
                                      { "Action", "SessionManagement" } });

                }

                 base.OnActionExecuting(filterContext);
                
            }
        }
    }

}

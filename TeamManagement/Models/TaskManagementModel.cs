﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DbIntegrationOracle;
using System.ComponentModel.DataAnnotations;


namespace TeamManagement.Models
{
    public class AssignedToAssignedByModel
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
    }

    public class TaskManagementModel
    {
        public List<TaskManagement> TaskDetailist { get; set; }
        public List<AssignedToAssignedByModel> UserDropDownList { get; set; }
        public string TeamName { get; set; }
        public string TeamLeadName { get; set; }
        public int UserID { get; set; }

        public string FullName { get; set; }
        public string Flag { get; set; }
        public int TeamId { get; set; }
        public int TeamLeadId { get; set; }
        public int DeleteTask(int pTaskId)
        {
            try
            {
                using (var entity = new OracleDBIntegrationEntities())
                {
                    var task = entity.TASKDETAILS.Where(x => x.TASKID == pTaskId).FirstOrDefault();
                    entity.TASKDETAILS.Remove(task);
                    entity.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public TaskManagementModel()
        {
            TaskDetailist = new List<TaskManagement>();
            TaskDetailist = GetTasks();

        }

        public String EditImagePath
        {
            get
            {
                return "../Images/1457616652_edit-file.png";
            }

        }

        public String SaveImagePath
        {
            get
            {
                return "../Images/1457620965_save_32.png";
            }

        }

        public List<TaskManagement> AddTaskDetail(TaskManagement pNewTask)
        {
            var taskDetails = new TaskManagementDetails();

            taskDetails.Id = pNewTask.Id;
            taskDetails.TaskDescription = pNewTask.TaskDescription;
            taskDetails.StartDate = pNewTask.StartDate;
            taskDetails.EndDate = pNewTask.EndDate;
            taskDetails.AssignedTo = pNewTask.AssignedTo;
            taskDetails.AssignedBy = pNewTask.AssignedBy;
            taskDetails.Completion = pNewTask.Completion;

            Integration.TaskUpdation(taskDetails);
            return GetTasks();
        }

        public List<TaskManagement> GetTasks()
        {
            var taskDetails = Integration.GetAllTasks();
            var allTasks = new List<TaskManagement>();

            foreach (var task in taskDetails)
            {
                var tasks = new TaskManagement();
                tasks.Id = (int)task.Id;
                tasks.TaskDescription = task.TaskDescription;
                if (task.StartDate == null)
                {
                    task.StartDate = DateTime.Now;
                }
                if (task.EndDate == null)
                {
                    task.EndDate = DateTime.Now;
                }
                tasks.strStartDate = task.StartDate.Value.ToString("MM-dd-yyyy");
                tasks.strEndDate = task.EndDate.Value.ToString("MM-dd-yyyy");
                tasks.AssignedBy = task.AssignedBy;
                tasks.AssignedTo = task.AssignedTo;
                tasks.Status = task.Status;
                tasks.Completion = task.Completion;
                allTasks.Add(tasks);
            }

            return allTasks;
        }

        public void EditTaskDetails(TaskManagement pTask)
        {
            using (var entity = new OracleDBIntegrationEntities())
            {
                var task = entity.TASKDETAILS.Where(x => x.TASKID == pTask.Id).FirstOrDefault();

                task.TASKDESC = pTask.TaskDescription;
                task.STARTDATE = pTask.StartDate;
                task.ENDDATE = pTask.EndDate;
                task.ASSIGNEDBY = pTask.AssignedBy;
                task.ASSIGNEDTO = pTask.AssignedTo;
                task.TASKSTATUS = pTask.Completion;

                entity.SaveChanges();
            }
        }

    }

    public class TaskManagement
    {
        public int Id { get; set; }
        public string TaskDescription { get; set; }
        public string Status { get; set; }

        [DisplayFormat(DataFormatString = "mm/dd/yyyy")]
        public DateTime? StartDate { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }

        [DisplayFormat(DataFormatString = "mm/dd/yyyy")]
        public DateTime? EndDate { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedTo { get; set; }
        public decimal? Completion { get; set; }

    }
}
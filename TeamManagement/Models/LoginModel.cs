﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamManagement.Models
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int ValidationStatus { get; set; }
    }
    public class LoginUserValues
    {
        public decimal UserId { get; set; }
        public string FullName { get; set; }
    }
}
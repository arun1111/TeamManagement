﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using Newtonsoft;
using DbIntegrationOracle;
using BusinessEntity;

namespace TeamManagement.Models.Team_Page
{
    public class MemberDetailsGet
    {

        public List<MemberDetails> lstgetMemDetails { get; set; }
        public int SaveStatus { get; set; }
    }

    public class TeamStructure
    {
        public List<TeamDetails> TeamTreeStructure { get; set; }

        public TeamStructure()
        {
            TeamTreeStructure = new List<TeamDetails>();
        }

        public List<TeamDetails> GetTeamDetails()
        {
            using (var entity = new OracleDBIntegrationEntities())
            {
                var teamDetailsList = (from team in entity.TEAMTBLs
                                       from user in entity.REGISTRATIONs
                                       where team.TEAMLEADID == user.ID
                                       select new TeamDetails
                                       {
                                           TeamId = team.TEAMID,
                                           TeamName = team.TEAMNAME,
                                           TeamLead = new UserDetails()
                                           {
                                               UserId = user.ID,
                                               UserName =
                                           user.FIRSTNAME + " " + user.LASTNAME
                                           },
                                           TeamMembers = (from members in entity.USERTEAMMAPPINGs
                                                          from users in entity.REGISTRATIONs
                                                          where team.TEAMID == members.TEAMID
                                                          && members.USERID == users.ID
                                                          select new UserDetails
                                                          {
                                                              UserId = members.USERID,
                                                              UserName = users.FIRSTNAME + " " + users.LASTNAME
                                                          }).ToList()
                                       }).ToList();

                TeamTreeStructure = teamDetailsList;
            }

            return TeamTreeStructure;
        }

        public void AddTeam(string pTeamName, decimal pTeamLeadId)
        {
            using (var entity = new OracleDBIntegrationEntities())
            {
                var newTeam = new TEAMTBL();
                decimal TopId = entity.TEAMTBLs.DefaultIfEmpty().Max(p => p == null ? 0 : p.TEAMID);
                newTeam.TEAMNAME = pTeamName;
                newTeam.TEAMLEADID = pTeamLeadId;
                newTeam.TEAMID = TopId + 1;
                entity.TEAMTBLs.Add(newTeam);
                entity.SaveChanges();
            }
        }

        public void AddMember(decimal pTeamId, string pMemberId)
        {

            using (var entity = new OracleDBIntegrationEntities())
            {
                string[] userid = pMemberId.Split(',');

                decimal TopId = entity.USERTEAMMAPPINGs.DefaultIfEmpty().Max(p => p == null ? 0 : p.MAPPINGID);
                for (int i = 0; i < userid.Length; i++)
                {

                    if (!string.IsNullOrEmpty(userid[i]))
                    {

                        var newMember = new USERTEAMMAPPING();
                        newMember.TEAMID = pTeamId;
                        newMember.MAPPINGID = TopId + 1;
                        newMember.USERID = Convert.ToInt32(userid[i]);
                        entity.USERTEAMMAPPINGs.Add(newMember);
                        TopId++;
                    }
                }

                entity.SaveChanges();

            }
        }

        public void TeamRename(decimal pTeamId, string pTeamRename)
        {
            using (var entity = new OracleDBIntegrationEntities())
            {
                var team = entity.TEAMTBLs.Where(x => x.TEAMID == pTeamId).FirstOrDefault();
                team.TEAMNAME = pTeamRename;
                entity.SaveChanges();
            }
        }

        public void DeleteMember(decimal pTeamId, decimal pMemberId)
        {
            using (var entity = new OracleDBIntegrationEntities())
            {
                var member = entity.USERTEAMMAPPINGs.Where(x => x.TEAMID == pTeamId && x.USERID == pMemberId).FirstOrDefault();
                entity.USERTEAMMAPPINGs.Remove(member);
                entity.SaveChanges();
            }
        }

        public void DeleteTeam(decimal pTeamId)
        {
            using (var entity = new OracleDBIntegrationEntities())
            {
                entity.USERTEAMMAPPINGs.Where(x => x.TEAMID == pTeamId).ToList().ForEach(i =>
                { entity.USERTEAMMAPPINGs.Remove(i); });

                var team = entity.TEAMTBLs.Where(x => x.TEAMID == pTeamId).FirstOrDefault();
                entity.TEAMTBLs.Remove(team);
                entity.SaveChanges();
            }
        }

    }

    public class TeamDetails
    {
        public decimal TeamId { get; set; }
        public string TeamName { get; set; }
        public UserDetails TeamLead { get; set; }
        public List<UserDetails> TeamMembers { get; set; }

        public TeamDetails()
        {
            TeamMembers = new List<UserDetails>();
        }

    }

    public class UserDetails
    {
        public decimal UserId { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
    }

}